<?php
return [
    'titles' => [
        'home' => 'Home',
        'id' => 'Id',
        'created_at' => 'Created At',
        'actions' => 'Actions',
        'modules' => 'Modules',
        'cache' => 'Cache',
        'entity' => 'Entity',
        'entities' => 'Entities',
        'alert' => 'Alert',
        'publish_accets' => 'Publish Accests',
        'access' => "Access",
    ],
    'labels' => [
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
        'published_at' => 'Published At',
        'deleted_at' => 'Deleted At'
    ],
    'menu' => [
        'single' => "Single",
        'core_modules' => "Core Modules",
        'access' => 'Access',
        'core' => "Core",
        'product' => 'Product',
    ],
    'buttons' => [
        'create' => 'Create',
        'save' => 'Save',
        'savencontinue' => 'Save & Continue',
        'cancel' => 'Cancel',
        'delete' => 'Delete',
        'update' => 'Update',
        'export' => 'Export',
    ],
    'comment' => [
        'per_page' => "Enter value comma-separated.",
        'comma_separated' => "Enter value comma-separated."
    ],
    'options' => [
        'yesno' => [
            'yes' => 'Yes',
            'no' => 'No'
        ],
        'status' => [
            'enable' => 'Enabled',
            'disable' => 'Disabled'
        ],
        'boolean' => [
            'true' => 'True',
            'false' => 'False'
        ],
    ],
    'modal' => [
        'title' => 'Confirmation',
        'confirmation-message' => 'Are you sure you want to delete this record?',
        'mass-delete-confirmation-message' => 'Are you sure you want to delete selected record(s)?',
    ],
    'messages' => [
        'select_record' => 'Please select atleast one record.',
        'no_records' => "No records available.",
        'module_create' => 'Module created successfully.',
        'module_update' => 'Module updated successfully.',
        'entity_create' => 'Entity created successfully.',
        'entity_update' => 'Entity updated successfully.',
        'mass_delete' => 'Are you sure to want to delete selected Records?',
        'module_update' => 'Module updated successfully.',
        'something_wrong' => 'Something went wrong, please try again after sometime.',
    ],
];
