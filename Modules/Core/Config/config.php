<?php

return [
    'name' => 'Core',
    /*
    |--------------------------------------------------------------------------
    | The prefix that'll be used for the administration
    |--------------------------------------------------------------------------
    */
    'admin_route_prefix' => 'admin',

    /*
    |--------------------------------------------------------------------------
    | Location where your themes are located
    |--------------------------------------------------------------------------
    */
    'themes_path' => base_path() . '/themes',

    /*
    |--------------------------------------------------------------------------
    | Which administration theme to use for the back end interface
    |--------------------------------------------------------------------------
    */
    'admin-theme' => 'admin',

    'skin' => 'skin-black',
    'default_per_page' => 20,
    'cache_expired_after' => 3600, // cache expired after seconds

    /*
    | common config options
    */
    'yes'               => 1,
    'no'                => 2,
    'defaultPerPage'    => 20,
];
