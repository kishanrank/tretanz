<?php

namespace Modules\Student\Sidebar;

use Modules\Core\Foundations\Menu;

class MenuSidebar
{
    protected $_menu;

    public function __construct()
    {
        $this->_menu = app(Menu::class);
        $this->initMenu();
    }

    public function initMenu()
    {
        $menuItems = [
            "group" => "core::core.menu.single.student",
            "title" => "student::student.menu.student",
            "route" => "admin.students.index",
            "icon" => "fas fa-users nav-icon",
            "active_actions" => [
                "admin.students.index"
            ],
            "order" => 10,
        ];
        $this->_menu->addMenuItem($menuItems);
    }
}
