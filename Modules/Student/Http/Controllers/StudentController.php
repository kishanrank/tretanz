<?php

namespace Modules\Student\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Student\Entities\Student;
use Modules\Student\Http\Requests\StoreStudentRequest;

class StudentController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            return view('student::admin.index');
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.dashboard.index', $e->getMessage());
        }
    }

    /**
     *  return dynamic response for jquery detatables
     */
    public function get(Request $request)
    {
        try {
            if ($request->ajax()) {
                $students = Student::latest()->get();

                return datatables()::of($students)
                    ->addIndexColumn()
                    ->editColumn('created_at', function ($student) {
                        return date('d-m-Y H:i:s', strtotime($student->created_at));
                    })
                    ->addColumn('action', function ($student) {
                        return $student->action_buttons;
                    })
                    ->rawColumns(['action', 'confirmed'])
                    ->make(true);
            }
            return $this->errorMessageResponse(['message' => trans('core::core.messages.something_wrong')]);
        } catch (\Throwable $e) {
            return $this->errorMessageResponse(['message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('student::admin.create');
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.students.index', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(StoreStudentRequest $request)
    {
        try {

            $data = $request->validated();
            $data['password'] = bcrypt($data['password']);
            $student = Student::create($data);
            if ($student && $student->id) {
                return $this->successRedirect('admin.students.index', 'Student created successfully.');
            }
            return $this->errorRedirect('admin.students.index', trans('core::core.messages.something_wrong'));
        } catch (\Throwable $e) {
            return $this->backWithError($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $student = Student::findOrFail($id);
            if ($student->delete()) {
                return $this->successRedirect('admin.students.index', 'Student deleted successfully.');
            }
            return $this->errorRedirect('admin.students.index', trans('core::core.messages.something_wrong'));
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.students.index', $e->getMessage());
        }
    }
}
