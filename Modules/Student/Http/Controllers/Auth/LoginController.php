<?php

namespace Modules\Student\Http\Controllers\Auth;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use ThrottlesLogins;

    public $maxAttempts = 5;

    public $decayMinutes = 3;

    public function __construct()
    {
        $this->middleware('guest:student')->except('logout');
    }

    public function showLoginForm()
    {
        return view('student::auth.login', [
            'title' => 'student Login',
            'loginRoute' => 'students.login',
        ]);
    }

    public function login(Request $request)
    {
        $this->validator($request);

        //check if the user has too many login attempts.
        if ($this->hasTooManyLoginAttempts($request)) {
            //Fire the lockout event
            $this->fireLockoutEvent($request);

            //redirect the user back after lockout.
            return $this->sendLockoutResponse($request);
        }

        if (Auth::guard('student')->attempt($request->only('username', 'password'), $request->filled('remember'))) {
            return $this->authenticated($request, Auth::guard('student')->user());
            // return redirect()->intended(route('admin.home'));
        }

        $this->incrementLoginAttempts($request);

        return $this->loginFailed();
    }

    protected function authenticated(Request $request, $user)
    {
        return redirect()->route('frontend.index');
    }

    public function logout()
    {
        Auth::guard('student')->logout();
        return redirect()
            ->route('students.login')
            ->with('success', 'Student has been logged out!');
    }

    private function validator(Request $request)
    {
        //validation rules.
        $rules = [
            'username' => 'required|exists:students|min:5|max:191',
            'password' => 'required|string|min:4|max:255',

        ];

        //custom validation error messages.
        $messages = [
            'username.exists' => 'These credentials do not match our records.',
        ];

        //validate the request.
        $request->validate($rules, $messages);
    }

    private function loginFailed()
    {
        $notification = array(
            'message' => 'Password Mismatch, please check credentials and try again!',
            'alert-type' => 'error'
        );
        return redirect()
            ->back()
            ->withInput()
            ->with($notification);
    }

    public function username()
    {
        return 'username';
    }
}
