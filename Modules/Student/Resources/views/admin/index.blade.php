@extends('theme::layouts.admin.master')

@section('title')
{{ trans('student::student.titles.manage_student') }}
@endsection

@section('extra-css')
@include('theme::asset.admin.css.datatable')
@stop

@section('content-header')
<!-- Content Header (Page header) -->
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ trans('student::student.titles.manage_student') }}</h1>
        </div>
        <div class="col-sm-6">
            <a class="btn btn-info btn-sm float-right" href="{{ route('admin.students.create') }}">Add New</a>
        </div>
    </div>
</div>
@stop

@section('content')
<!-- Main content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info card-outline">
                <div class="card-body">
                    <div class="table-responsive data-table-wrapper">
                        <table id="students-table" class="table table-condensed table-hover table-bordered" width="100%">
                            <thead class="transparent-bg">
                                <tr>
                                    <th>{{ trans('student::student.grid.header.no')}}</th>
                                    <th>{{ trans('student::student.grid.header.name')}}</th>
                                    <th>{{ trans('student::student.grid.header.username')}}</th>
                                    <th>{{ trans('student::student.grid.header.created_at')}}</th>
                                    <th>{{ trans('student::student.grid.header.action')}}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                @include('theme::layouts.admin.modals.confirm')
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@stop

@section('after-js')
@include('theme::asset.admin.js.datatable')
<script>
    $(function() {
        var dataTable = $('#students-table').DataTable({
            serverSide: true,
            ajax: {
                url: '{{ route("admin.students.get") }}',
                type: 'post'
            },
            lengthMenu: [20, 30, 50, 100, 200],
            pageLength: 20,
            columns: [
                {
                    data: 'DT_RowIndex'
                },
                {
                    data: 'name'
                },
                {
                    data: 'username'
                },
                {
                    data: 'created_at'
                },
                {
                    data: 'action',
                    searchable: false,
                    sortable: false
                },
            ],
            searchDelay: 500,
        });
    });
</script>
@stop