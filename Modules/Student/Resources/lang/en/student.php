<?php

return [
    'titles' => [
        'manage_student' => 'Manage Students',
        'create_student' => 'Create Students',
    ],
    'menu' => [
        'student' => 'Manage Students',
    ],
    'buttons' => [
        'cancel' => 'Cancel',
        'create' => 'Create New',
        'save' => 'Save',
        'update' => 'Update',
        'delete' => 'Delete',
        'back' => 'Back'
    ],
    'grid' => [
        'header' => [
            'no' => 'No.',
            'name' => 'Name',
            'username'   => 'User Name',
            'created_at' => 'Created At',
            'action'     => 'Actions'
        ],
    ],
    'form' => [
        'label' => [
            'name' => 'Name : ',
            'username' => 'User Name : ',
            'password' => 'Password : ',
            'password_confirmation' => 'Password Confirmation : ',
        ],
        'input' => [],
        'placeholder' => [
            'name' => "Enter Name",
            'username' => 'Enter User Name',
            'password' => 'Enter Password',
            'password_confirmation' => 'Enter Confirmed Password'
        ],
    ],
];
