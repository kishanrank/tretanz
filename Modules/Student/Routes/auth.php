<?php

use Illuminate\Support\Facades\Route;

Route::name('students.')->prefix('students')->group(function () {
    //Login Routes
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login')->name('postLogin');
    Route::post('/logout', 'LoginController@logout')->name('logout');
});