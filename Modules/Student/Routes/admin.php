<?php

use Illuminate\Support\Facades\Route;

Route::prefix('students')->group(function () {
    Route::get('/', [
        'as' => 'admin.students.index',
        'uses' => 'StudentController@index',
        'middleware' => 'auth:admin'
    ]);

    Route::post('/get', [
        'as' => 'admin.students.get',
        'uses' => 'StudentController@get',
        'middleware' => 'auth:admin'
    ]);
    Route::get('/create', [
        'as' => 'admin.students.create',
        'uses' => 'StudentController@create',
        'middleware' => 'auth:admin'
    ]);
    Route::post('/store', [
        'as' => 'admin.students.store',
        'uses' => 'StudentController@store',
        'middleware' => 'auth:admin'
    ]);
    Route::get('/delete/{id}', [
        'as' => 'admin.students.destroy',
        'uses' => 'StudentController@destroy',
        'middleware' => 'auth:admin'
    ]);
});