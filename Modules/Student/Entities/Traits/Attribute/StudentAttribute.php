<?php

namespace Modules\Student\Entities\Traits\Attribute;

trait StudentAttribute
{
    public function getActionButtonsAttribute()
    {
        $data =  '<div class="btn-group action-btn">';
        $data .= $this->getDeleteButtonAttribute('admin.students.destroy');
        $data .= '</div>';
        return $data;
    }
}
