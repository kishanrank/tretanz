<?php

namespace Modules\Student\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Core\Entities\Traits\BaseModelTrait;
use Modules\Student\Entities\Traits\Attribute\StudentAttribute;

class Student extends Authenticatable
{
    use Notifiable;
    use StudentAttribute;
    use BaseModelTrait;

    protected $fillable = ['name', 'username', 'password'];

}
