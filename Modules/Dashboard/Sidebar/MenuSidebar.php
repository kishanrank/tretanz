<?php

namespace Modules\Dashboard\Sidebar;

use Modules\Core\Foundations\Menu;

class MenuSidebar
{
    protected $_menu;

    public function __construct()
    {
        $this->_menu = app(Menu::class);
        $this->initMenu();
    }

    public function initMenu()
    {
        $menuItems = [
            "group" => "core::core.menu.single.dashboard",
            "title" => "dashboard::dashboard.titles.dashboard",
            "route" => "admin.dashboard.index",
            "icon" => "fas fa-tachometer-alt nav-icon",
            "active_actions" => [
                "admin.dashboard.index"
            ],
            "order" => 1,
        ];
        $this->_menu->addMenuItem($menuItems);
    }
}
