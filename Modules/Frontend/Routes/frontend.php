<?php

use Illuminate\Support\Facades\Route;

Route::prefix('students')->group(function () {
    Route::get('/home', [
        'as' => 'frontend.index',
        'uses' => 'IndexController@index',
        'middleware' => 'auth:student'
    ]);

    Route::post('/start-quiz', [
        'as' => 'frontend.startquiz',
        'uses' => 'IndexController@startQuiz',
        'middleware' => 'auth:student'
    ]);

    Route::post('/store-answer', [
        'as' => 'frontend.answer.store',
        'uses' => 'IndexController@storeAnswer',
        'middleware' => 'auth:student'
    ]);

    Route::get('/quiz-complete', [
        'as' => 'frontend.quiz.complete',
        'uses' => 'IndexController@complete',
        'middleware' => 'auth:student'
    ]);

    Route::get('/check-result', [
        'as' => 'frontend.checkresult',
        'uses' => 'IndexController@checkResult',
        'middleware' => 'auth:student'
    ]);
});
