@extends('theme::layouts.frontend.master')

@section('content')
    <div>
        <div class="col-lg-offset-5">
            <h2>Id : {{ auth()->guard('student')->user()->id }}</h2>
            <h3>Your total mark : <b><span style="color: green">{{ Session::get('score') ?? 0 }}</span></b></h3>
        </div>
        <hr>

        @foreach ($answeredQuestion as $answerQ)
            <div class="col-md-6 col-lg-8 col-sm-6 col-lg-offset-4">
                <h3><span style="color: red">Question : </span> {{ $answerQ->question }} ?</h3>
                <div class="col-lg-offset-2">
                    <div class="form-group">
                        <p type="text" name="selected_answer">Your Choice Was : {{ $answerQ->selected_answer }}</p>
                    </div>
                    <div class="form-group">
                        <p type="text" name="right_answer"><span style="color: green">Right Answer :
                                {{ $answerQ->right_answer }}</span></p>
                    </div>

                </div>
            </div>
        @endforeach

    </div>
@endsection
