@extends('theme::layouts.frontend.master')

@section('content')
    <div class="col-md-6 col-lg-6 col-sm-6 col-lg-offset-3">

        <h4>Quiz has been completed now.</h4>

        <h4>Your score is {{ session()->get('score')  ?? 0 }}</h4>

        <a href="{{ route('frontend.checkresult') }}">Check Results</a>
    </div>
@endsection
