@extends('theme::layouts.frontend.master')

@push('script')
    <script type="text/javascript">
        var timeoutHandle;

        function quizCountDown(minutes) {
            var seconds = 60;
            var mins = minutes

            function tick() {
                var counter = document.getElementById("timer");
                var current_minutes = mins - 1
                seconds--;
                counter.innerHTML =
                    current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds);
                if (seconds > 0) {
                    timeoutHandle = setTimeout(tick, 1000);
                } else {
                    if (mins > 1) {
                        setTimeout(function() {
                            quizCountDown(mins - 1);
                        }, 1000);

                    }
                }
            }
            tick();
        }

        quizCountDown('<?php echo $quiz->total_time; ?>');
    </script>

    <script type="text/javascript">
        var time = '<?php echo $quiz->total_time; ?>';
        var realtime = time * 60000;
        setTimeout(function() {
                alert('Time Out');
                window.location.href = "{{ route('frontend.quiz.complete') }}";
            },
            realtime);
    </script>
@endpush

@section('content')

    <body style="background-color: darkseagreen">
        <div>
            <nav class="col-lg-1 pull-right">
                <div class="sidebar-nav-fixed affix">
                    <h1><b>Time <span id="timer" style="color: red">0.00</span></b></h1><br>
                </div>
            </nav>
            <h1 class="col-lg-offset-4" style="color: red;"><span
                    style="background-color:seagreen;color: white;border-radius: 5px"><b> Quiz Name :
                        {{ $quiz->quiz_name }}
                    </b></span></h1>
            <a href="{{ route('frontend.quiz.complete') }}">Submit </a>
            <div class="col-md-6 col-lg-6 col-sm-6 col-lg-offset-3" style="background-color: white">
                @foreach ($questions as $question)
                    <div class="col-md-6 col-lg-8 col-sm-6 col-lg-offset-2">
                        <form method="post" action="{{ route('frontend.answer.store') }}" class="quiz-answer">
                            {{ csrf_field() }}

                            <h3>{{ $question->question }} ?</h3>
                            <div class="col-lg-offset-1">
                                <input type="hidden" name="question" value="{{ $question->question }}">
                                <input type="hidden" name="id" value="{{ $question->id }}">
                                <input type="hidden" name="quiz_id" value="{{ $question->quiz_id }}">
                                <input name="answer" value="{{ $question->option_1 }}" type="radio">
                                {{ $question->option_1 }}
                                <br>
                                <input name="answer" value="{{ $question->option_2 }}"
                                    type="radio">{{ $question->option_2 }}<br>
                                <input name="answer" value="{{ $question->option_3 }}"
                                    type="radio">{{ $question->option_3 }}<br>
                                <input name="answer" value="{{ $question->option_4 }}"
                                    type="radio">{{ $question->option_4 }}<br>
                                <input type="submit" name="submit" value="submit" class="btn btn-primary" id="submitbtn">
                            </div>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>
    </body>
@endsection


@push('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        $('.quiz-answer').on('submit', function(e) {
            var form = $(this);
            var submit = form.find("[type=submit]");
            var submitOriginalText = submit.attr("value");

            e.preventDefault();
            var data = form.serialize();
            var url = form.attr('action');
            var post = form.attr('method');
            $.ajax({
                type: post,
                url: url,
                data: data,
                success: function(data) {
                    submit.attr("value", "Submitted");
                },
                beforeSend: function() {
                    submit.attr("value", "Loading...");
                    submit.prop("disabled", true);
                },
                error: function() {
                    submit.attr("value", submitOriginalText);
                    submit.prop("disabled", false);
                    // show error to end user
                }
            })
        })
    </script>
@endpush
