@extends('theme::layouts.frontend.master')

@section('content')
    <form method="post" action="{{ route('frontend.startquiz') }}">
        {{ csrf_field() }}

        <div class="col-md-6 col-lg-6 col-sm-6 col-lg-offset-3">
            <div class="form-group">
                <label class="col-form-label" for="quiz_id">Unique Quiz Id:</label>
                <input type="text" name="quiz_id" class="form-control" id="quiz_id"
                    placeholder="E.g. 123SDFESJ" required>
            </div>
            <button type="Submit" class="btn btn-success btn-block">Start Quiz</button><br><br>
            <h4 style="color: red">Please keep in mind Quiz will autosubmit after given time.</h4>
        </div>

    </form>
@endsection
