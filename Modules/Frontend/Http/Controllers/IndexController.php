<?php

namespace Modules\Frontend\Http\Controllers;

use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Quiz\Entities\Answer;
use Modules\Quiz\Entities\Question;
use Modules\Quiz\Entities\Quiz;
use Modules\Student\Entities\Student;

class IndexController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        try {
            return view('frontend::index');
        } catch (\Throwable $th) {
            return view('theme::layouts.frontend.errors.404');
        }
    }

    public function startQuiz(Request $request)
    {
        try {
            $quizId = $request->input('quiz_id');

            $quiz = Quiz::where('unique_id', $quizId)->get()->first();

            if (!$quiz) {
                throw new Exception("Invalid Quiz Id.");
            }
            $student = Student::find(Auth::guard('student')->user()->id);
            $completedQuiz = Student::whereId($student->id)->value('completed_quiz');
            $completedQuiz = unserialize($completedQuiz);

            if ($completedQuiz && in_array($quiz->id, $completedQuiz)) {
                throw new Exception("Quiz already attempted.");
            }

            $completedQuiz[] = $quiz->id;

            $student->completed_quiz = serialize($completedQuiz);
            $student->save();

            session()->forget('score');
            $questions = Question::where('quiz_id', $quiz->id)->get();
            return view('frontend::quiz', compact('questions', 'quiz'));
        } catch (\Throwable $th) {
            return $this->errorRedirect('frontend.index', $th->getMessage());
        }
    }

    public function storeAnswer(Request $request)
    {
        if ($request->ajax()) {
            $rightAnswer = Question::whereId($request->id)->value('answer');
            $answer = Answer::create([
                'student_id' => Auth::guard('student')->user()->id,
                'quiz_id' => $request->input('quiz_id'),
                'question' => $request->input('question'),
                'selected_answer' => $request->input('answer'),
                'right_answer' => $rightAnswer
            ]);

            if ($rightAnswer == $request->input('answer')) {
                Log::info('asss');
                $currentScore = Session::get('score');
                Session::put('score', $currentScore + 1);
            }
            return response($answer);
        } else {
            return $this->errorMessageResponse(['message' => 'Something went wrong.']);
        }
    }

    public function complete()
    {
        return view('frontend::quiz-complete');
    }

    public function checkResult()
    {
        $answeredQuestion = Answer::where('student_id', Auth::guard('student')->user()->id)->get();
        return view('frontend::result', compact('answeredQuestion'));
    }
}
