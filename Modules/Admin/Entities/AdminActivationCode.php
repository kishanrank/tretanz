<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class AdminActivationCode extends Model
{
    protected $table = 'admin_activation_code';

    protected $fillable = ['code'];

    public function getRouteKeyName()
    {
        return 'code';
    }

    public function admin() {
        return $this->belongsTo(Admin::class);
    }
}
