<?php

namespace Modules\Admin\Entities\Traits\Admin\Attribute;

use Modules\Admin\Entities\Admin;

trait AdminAttribute
{
    public function getActionButtonsAttribute()
    {
        $data =  '<div class="btn-group action-btn">';
        $data .= $this->getEditButtonAttribute('admin.adminuser.edit');
        if ($this->id != 1) {
            $data .= $this->getDeleteButtonAttribute('admin.adminuser.destroy');
        }
        if ($this->confirmed == 0) {
            $data .= $this->getSendVerificationEmailAttribute('admin.adminuser.destroy');
        }
        $data .= '</div>';
        return $data;
    }

    public function getSendVerificationEmailAttribute($route)
    {
        return '<a href="' . route('adminuser.resend.code') . '?email=' . $this->email . '" title="Send Verification Email" class="btn btn-xs btn-flat btn-primary send-veri-email"><i class="fa fa-paper-plane"></i></a>';
    }

    public function getConfirmedForGridAttribute()
    {
        if ($this->confirmed) {
            return '<label class="badge badge-success">' . Admin::CONFIRMED_YES_TEXT . '</label>';
        }
        return '<label class="badge badge-danger">' . Admin::CONFIRMED_NO_TEXT . '</label>';
    }
}
