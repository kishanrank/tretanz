<?php

namespace Modules\Admin\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ProvidersEventServiceProvider;
use Illuminate\Support\ServiceProvider;

class EventServiceProvider extends ProvidersEventServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Modules\Admin\Events\Admin\ActivationCodeEvent' => [
            'Modules\Admin\Listeners\Admin\ActivationCodeListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
