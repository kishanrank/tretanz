<?php

namespace Modules\Admin\Http\Controllers\Auth;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Modules\Admin\Entities\Admin;
use Modules\Admin\Notifications\Admin\SendActivationCode as AdminSendActivationCode;

class RegisterController extends Controller
{
    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function showRegisterForm()
    {
        return view('admin::auth.register', [
            'title' => 'Admin Register',
            'registerRoute' => 'admin.postRegister',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function register(Request $request)
    {
        $this->validator($request);
        $user = $this->create($request->all());
        $user->adminActivationCode()->create([
            'code' => Str::random(128)
        ]);
        $this->guard('admin')->logout();
        $code = $user->adminActivationCode;
        $url = route('admin.activate.account', ['code' => $code->code]);
        Notification::send($user, new AdminSendActivationCode($url));
        $notification = array(
            'message' => 'Thank you for Registering with us. please verify your account to get access.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.login')->with($notification);
    }

    protected function validator(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email'    => 'required|email|min:5|max:191|unique:admins,email',
            'password' => 'required|string|confirmed|min:4|max:255',
        ];
        $request->validate($rules);
    }

    protected function create(array $data)
    {
        $user = Admin::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);

        return $user;
    }
}
