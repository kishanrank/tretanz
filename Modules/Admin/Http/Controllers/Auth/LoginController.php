<?php

namespace Modules\Admin\Http\Controllers\Auth;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    use ThrottlesLogins;

    public $maxAttempts = 5;

    public $decayMinutes = 3;

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm()
    {
        return view('admin::auth.login', [
            'title' => 'Admin Login',
            'loginRoute' => 'admin.login',
            'forgotPasswordRoute' => 'admin.password.request',
        ]);
    }

    public function login(Request $request)
    {

        // echo "<pre>";
        // print_r($request->all());die;
        $this->validator($request);

        //check if the user has too many login attempts.
        if ($this->hasTooManyLoginAttempts($request)) {
            //Fire the lockout event
            $this->fireLockoutEvent($request);

            //redirect the user back after lockout.
            return $this->sendLockoutResponse($request);
        }

        if (Auth::guard('admin')->attempt($request->only('email', 'password'), $request->filled('remember'))) {
            return $this->authenticated($request, Auth::guard('admin')->user());
        }

        $this->incrementLoginAttempts($request);

        return $this->loginFailed();
    }

    protected function authenticated(Request $request, $admin)
    {
        if (!$admin->isAdminActivated()) {
            Auth::guard('admin')->logout();
            $notification = array(
                'message' => 'Your account is not activated yet please activate first. </br> Need a link? <a class="resend-link" href="' . route('admin.resend.code') . '?email=' . $admin->email . '">Resend Link</a>',
                'alert-type' => 'error'
            );
            return redirect(route('admin.login'))->with($notification);
        }
        return redirect()->route('admin.dashboard.index');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()
            ->route('admin.login')
            ->with('success', 'Admin has been logged out!');
    }

    private function validator(Request $request)
    {
        //validation rules.
        $rules = [
            'email'    => 'required|email|exists:admins|min:5|max:191',
            'password' => 'required|string|min:4|max:255',

        ];

        //custom validation error messages.
        $messages = [
            'email.exists' => 'These credentials do not match our records.',
        ];

        //validate the request.
        $request->validate($rules, $messages);
    }

    private function loginFailed()
    {
        $notification = array(
            'message' => 'Password Mismatch, please check credentials and try again!',
            'alert-type' => 'error'
        );
        return redirect()
            ->back()
            ->withInput()
            ->with($notification);
    }

    public function username()
    {
        return 'email';
    }
}
