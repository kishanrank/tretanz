@if (session()->has('success'))
    <div class="alert alert-dismissable alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
            {{!! session()->get('success') !!}}
        </strong>
    </div>
@endif

@if (Session::has('success'))
    <div class="callout callout-success">
        <button type="button" class="close close-msg-div" data-dismiss="alert" aria-hidden="true">&times; </i></button>
        <p>{{ Session::get('success') }}</p>
    </div>
@endif

@if (Session::has('error'))
  <div class="callout callout-danger">
        <button type="button" class="close close-msg-div" data-dismiss="alert" aria-hidden="true">&times; </i></button>
        <p>{{ Session::get('error') }}</p>
    </div>
@endif

@if (Session::has('warning'))
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session::get('warning') }}
    </div>
@endif

@if (Session::has('info'))
    <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session::get('info') }}
    </div>
@endif
<style type="text/css">
    .callout .callout-success{
        border-left-color: #28a745;
    }
    .callout .callout-danger{
        border-left-color: red;
    }
</style>
@push('after-js-stack')
<script type="text/javascript">
    jQuery(document).ready(function() {
        $('.close-msg-div').click(function(){
            $('.callout').hide();
        });
    });
</script>
@endpush