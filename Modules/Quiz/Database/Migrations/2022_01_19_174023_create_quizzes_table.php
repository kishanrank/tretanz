<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Quiz\Entities\Quiz;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $quiz = new Quiz();
        Schema::create($quiz->getTable(), function (Blueprint $table) {
            $table->id();
            $table->string('quiz_name');
            $table->string('unique_id');
            $table->integer('total_question');
            $table->integer('total_time');
            $table->integer('total_mark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $quiz = new Quiz();
        Schema::dropIfExists($quiz->getTable());
    }
}
