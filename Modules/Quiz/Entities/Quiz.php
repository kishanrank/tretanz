<?php

namespace Modules\Quiz\Entities;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $table = 'quizzes';

    protected $fillable = ['quiz_name', 'unique_id', 'total_question', 'total_time', 'total_mark'];
}
