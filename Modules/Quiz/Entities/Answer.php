<?php

namespace Modules\Quiz\Entities;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['student_id', 'quiz_id', 'question', 'selected_answer', 'right_answer', ''];
}
