<?php

namespace Modules\Quiz\Entities;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['quiz_id', 'question', 'option_1', 'option_2', 'option_3', 'option_4', 'answer'];
}
