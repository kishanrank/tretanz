<?php

use Illuminate\Support\Facades\Route;

Route::prefix('quizzes')->group(function () {
    Route::get('/', [
        'as' => 'admin.quizzes.index',
        'uses' => 'QuizController@index',
        'middleware' => 'auth:admin'
    ]);
    Route::post('/get', [
        'as' => 'admin.quizzes.get',
        'uses' => 'QuizController@get',
        'middleware' => 'auth:admin'
    ]);
    Route::get('/create', [
        'as' => 'admin.quizzes.create',
        'uses' => 'QuizController@create',
        'middleware' => 'auth:admin'
    ]);
    Route::post('/store', [
        'as' => 'admin.quizzes.store',
        'uses' => 'QuizController@store',
        'middleware' => 'auth:admin'
    ]);
});

Route::prefix('questions')->group(function () {
    Route::get('/', [
        'as' => 'admin.questions.index',
        'uses' => 'QuestionController@index',
        'middleware' => 'auth:admin'
    ]);
    Route::post('/get', [
        'as' => 'admin.questions.get',
        'uses' => 'QuestionController@get',
        'middleware' => 'auth:admin'
    ]);
    Route::get('/create', [
        'as' => 'admin.questions.create',
        'uses' => 'QuestionController@create',
        'middleware' => 'auth:admin'
    ]);
    Route::post('/store', [
        'as' => 'admin.questions.store',
        'uses' => 'QuestionController@store',
        'middleware' => 'auth:admin'
    ]);
});