<?php

return [
    'titles' => [
        'manage_quiz' => 'Manage Quiz',
        'create_quiz' => 'Create Quiz',
        'create_question' => 'Create Question'
    ],
    'menu' => [
        'quiz' => 'Manage Quiz',
    ],
    'buttons' => [
        'cancel' => 'Cancel',
        'create' => 'Create New',
        'create_quiz' => 'Create Quiz & Continue',
        'create_question_next' => 'Create Question & Next',
        'save' => 'Save',
        'update' => 'Update',
        'delete' => 'Delete',
        'back' => 'Back'
    ],
    'grid' => [
        'header' => [
            'no' => 'No.',
            'name' => 'Name',
            'username'   => 'User Name',
            'created_at' => 'Created At',
            'action'     => 'Actions'
        ],
    ],
    'form' => [
        'label' => [
            'quiz_name' => 'Quiz Name : ',
            'total_question' => 'Total Question : ',
            'total_time' => 'Total Time : ',
            'total_mark' => 'Total Mark : ',
            'question' => 'Question : ',
            'option_1' => 'Option 1 : ',
            'option_2' => 'Option 2 : ',
            'option_3' => 'Option 3 : ',
            'option_4' => 'Option 4 : ',
            'answer' => 'Answer'
        ],
        'input' => [],
        'placeholder' => [
            'quiz_name' => 'Enter Quiz Name',
            'total_question' => 'Enter Total Question',
            'total_time' => 'Enter Total Time',
            'total_mark' => 'Enter Total Mark',
            'question' => 'Enter Question',
            'option_1' => 'Enter Option 1',
            'option_2' => 'Enter Option 2',
            'option_3' => 'Enter Option 3',
            'option_4' => 'Enter Option 4',
            'answer' => 'Enter Answer'
        ],
    ],
];
