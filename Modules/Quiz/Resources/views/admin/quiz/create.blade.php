@extends('theme::layouts.admin.master')

@section('title')
{{ trans('quiz::quiz.titles.create_quiz') }}
@endsection

@section('extra-css')
@stop

@section('content-header')
<!-- Content Header (Page header) -->
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ trans('quiz::quiz.titles.create_quiz') }}</h1>
        </div>
        <div class="col-sm-6">
            <div class="float-right">
            </div>
        </div>
    </div>
</div>
<!-- /.content-header -->
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            {{ Form::open(['route' => 'admin.quizzes.store', 'class' => "form-horizontal", 'method' => 'post', 'id' => 'create-quizzes'])}}
            <div class="card card-info card-outline">
                <div class="card-header with-border">
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        {{ Form::label('quiz_name', trans('quiz::quiz.form.label.quiz_name'), ['class' => 'col-md-2 control-label label-right required']) }}

                        <div class="col-md-10">
                            {{ Form::text('quiz_name', null, ['class' => 'form-control box-size', 'id' => 'quiz_name', 'autocomplete' => 'off', 'placeholder' => trans('quiz::quiz.form.placeholder.quiz_name'), 'required' => 'required']) }}
                            @error('quiz_name')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('total_question', trans('quiz::quiz.form.label.total_question'), ['class' => 'col-md-2 control-label label-right required']) }}

                        <div class="col-md-10">
                            {{ Form::number('total_question', null, ['class' => 'form-control box-size', 'id' => 'total_question', 'autocomplete' => 'off', 'placeholder' => trans('quiz::quiz.form.placeholder.total_question'), 'required' => 'required']) }}
                            @error('total_question')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('total_time', trans('quiz::quiz.form.label.total_time'), ['class' => 'col-md-2 control-label label-right required']) }}

                        <div class="col-md-10">
                            {{ Form::number('total_time', null, ['class' => 'form-control box-size', 'id' => 'total_time', 'autocomplete' => 'off', 'placeholder' => trans('quiz::quiz.form.placeholder.total_time'), 'required' => 'required']) }}
                            @error('total_time')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('total_mark', trans('quiz::quiz.form.label.total_mark'), ['class' => 'col-md-2 control-label label-right required']) }}

                        <div class="col-md-10">
                            {{ Form::number('total_mark', null, ['class' => 'form-control box-size', 'id' => 'total_mark', 'autocomplete' => 'off', 'placeholder' => trans('quiz::quiz.form.placeholder.total_mark'), 'required' => 'required']) }}
                            @error('total_mark')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="edit-form-btn">
                        {{ Form::submit(trans('quiz::quiz.buttons.create_quiz'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            {{ Form::close()}}
        </div>
    </div>
</div>
@stop

@section('after-js')
@stop