@extends('theme::layouts.admin.master')

@section('title')
{{ trans('quiz::quiz.titles.create_question') }}
@endsection

@section('extra-css')
@stop

@section('content-header')
<!-- Content Header (Page header) -->
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ trans('quiz::quiz.titles.create_question') }}</h1>
        </div>
        <div class="col-sm-6">
            <div class="float-right">
            </div>
        </div>
    </div>
</div>
<!-- /.content-header -->
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            {{ Form::open(['route' => 'admin.questions.store', 'class' => "form-horizontal", 'method' => 'post', 'id' => 'create-question'])}}
            <div class="card card-info card-outline">
                <div class="card-header with-border">
                    Current Question No. : {{$currentQuestion}}
                </div>

                <div class="card-body">
                    <input type="hidden" name="quiz_id" value="{{ $quiz->id }}" readonly>
                    <div class="form-group row">
                        {{ Form::label('question', trans('quiz::quiz.form.label.question'), ['class' => 'col-md-2 control-label label-right required']) }}

                        <div class="col-md-10">
                            {{ Form::text('question', null, ['class' => 'form-control box-size', 'id' => 'question', 'autocomplete' => 'off', 'placeholder' => trans('quiz::quiz.form.placeholder.question'), 'required' => 'required']) }}
                            @error('question')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('option_1', trans('quiz::quiz.form.label.option_1'), ['class' => 'col-md-2 control-label label-right required']) }}

                        <div class="col-md-10">
                            {{ Form::text('option_1', null, ['class' => 'form-control box-size', 'id' => 'option_1', 'autocomplete' => 'off', 'placeholder' => trans('quiz::quiz.form.placeholder.option_1'), 'required' => 'required']) }}
                            @error('option_1')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('option_2', trans('quiz::quiz.form.label.option_2'), ['class' => 'col-md-2 control-label label-right required']) }}

                        <div class="col-md-10">
                            {{ Form::text('option_2', null, ['class' => 'form-control box-size', 'id' => 'option_2', 'autocomplete' => 'off', 'placeholder' => trans('quiz::quiz.form.placeholder.option_2'), 'required' => 'required']) }}
                            @error('option_2')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('option_3', trans('quiz::quiz.form.label.option_3'), ['class' => 'col-md-2 control-label label-right required']) }}

                        <div class="col-md-10">
                            {{ Form::text('option_3', null, ['class' => 'form-control box-size', 'id' => 'option_3', 'autocomplete' => 'off', 'placeholder' => trans('quiz::quiz.form.placeholder.option_3'), 'required' => 'required']) }}
                            @error('option_3')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('option_4', trans('quiz::quiz.form.label.option_4'), ['class' => 'col-md-2 control-label label-right required']) }}

                        <div class="col-md-10">
                            {{ Form::text('option_4', null, ['class' => 'form-control box-size', 'id' => 'option_4', 'autocomplete' => 'off', 'placeholder' => trans('quiz::quiz.form.placeholder.option_4'), 'required' => 'required']) }}
                            @error('option_4')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('answer', trans('quiz::quiz.form.label.answer'), ['class' => 'col-md-2 control-label label-right required']) }}

                        <div class="col-md-10">
                            {{ Form::text('answer', null, ['class' => 'form-control box-size', 'id' => 'answer', 'autocomplete' => 'off', 'placeholder' => trans('quiz::quiz.form.placeholder.answer'), 'required' => 'required']) }}
                            @error('answer')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>


                    <div class="edit-form-btn">
                        {{ Form::submit(trans('quiz::quiz.buttons.create_question_next'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            {{ Form::close()}}
        </div>
    </div>
</div>
@stop

@section('after-js')
@stop