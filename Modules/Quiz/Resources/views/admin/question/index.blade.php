@extends('theme::layouts.admin.master')

@section('title')
    {{ 'Manage Question' }}
@endsection

@section('content-header')
    <!-- Content Header (Page header) -->
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ 'Manage Question' }}</h1>
            </div>
            <div class="col-sm-6"></div>
        </div>
    </div>
    <!-- /.content-header -->
@stop

@section('content')
    <!-- Main content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-edit"></i>
                            {{ "Manage Questions" }}
                        </h3>
                    </div>
                    <div class="card-body">
                        <p>All questions data saved successfully.</p>
                        <p>Your Quiz Unique Id is : <b>{{ request()->get('uniqueId') }} </b>, Please note it down.</p>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop
