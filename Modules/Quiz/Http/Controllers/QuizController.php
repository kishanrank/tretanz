<?php

namespace Modules\Quiz\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Quiz\Entities\Quiz;
use Modules\Quiz\Http\Requests\StoreQuizRequest;

class QuizController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('quiz::admin.quiz.create');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('quiz::admin.quiz.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreQuizRequest $request)
    {
        $data = $request->validated();
        $data['unique_id'] = md5(microtime().rand());
        $quiz = Quiz::create($data);

        return view('quiz::admin.question.create', ['quiz' => $quiz, 'currentQuestion' => 1]);

    }
}
