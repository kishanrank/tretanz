<?php

namespace Modules\Quiz\Http\Controllers;

use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Quiz\Entities\Question;
use Modules\Quiz\Entities\Quiz;
use Modules\Quiz\Http\Requests\StoreQuestionRequest;

class QuestionController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('quiz::admin.question.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('quiz::admin.question.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreQuestionRequest $request)
    {
        try {
            $questionData = $request->validated();
            // print_r($questionData);die;
            $question = new Question();
            $question->quiz_id = $request->quiz_id;
            $question->question = $request->question;
            $question->option_1 = $request->option_1;
            $question->option_2 = $request->option_2;
            $question->option_3 = $request->option_3;
            $question->option_4 = $request->option_4;
            $question->answer = $request->answer;
            $question->save();

            $questionCount = Question::where('quiz_id', '=', $question->quiz_id)->count();
            $totalSelectedQuestion = Quiz::where('id', '=', $question->quiz_id)->value('total_question');

            if ($questionCount < $totalSelectedQuestion) {
                $quiz = Quiz::find($question->quiz_id);
                return view('quiz::admin.question.create', ['quiz' => $quiz, 'currentQuestion' => $questionCount]);
            } else {
                $uniqueId = Quiz::where('id', '=', $question->quiz_id)->value('unique_id');
                return redirect()->action(
                    [QuestionController::class, 'index'], ['uniqueId' => $uniqueId]
                );
            }
        } catch (Exception $e) {
            return $this->errorRedirect('admin.dashboard.index', $e->getMessage());
        }
    }
}
