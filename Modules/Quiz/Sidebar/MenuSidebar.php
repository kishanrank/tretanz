<?php

namespace Modules\Quiz\Sidebar;

use Modules\Core\Foundations\Menu;

class MenuSidebar
{
    protected $_menu;

    public function __construct()
    {
        $this->_menu = app(Menu::class);
        $this->initMenu();
    }

    public function initMenu()
    {
        $menuItems = [
            "group" => "core::core.menu.single.quiz",
            "title" => "quiz::quiz.menu.quiz",
            "route" => "admin.quizzes.index",
            "icon" => "fas fa-users nav-icon",
            "active_actions" => [
                "admin.quizzes.index"
            ],
            "order" => 10,
        ];
        $this->_menu->addMenuItem($menuItems);
    }
}
